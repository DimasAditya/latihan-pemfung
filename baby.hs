doubleme x = x + x
doubleus x y = doubleme x + doubleme y
boomBangs xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, even x]
mystery xs = foldr (++) [] (map sing xs) where sing x = [x]
maxtiga x y z = if x>y && x>z
			then return x
		else if y>x && y>z
			then return y
		else 
			return z 
myflip x y = y x

seto xs ys = concat (map (\x -> map(\y -> (x,y))ys)xs)
seto2 xs ys = (\x -> map(\y -> (x,y))ys)


sumi xs = foldl (+) 0 xs
sumi2 xs = foldr (+) 0 xs

quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs,
                                  y <= x]
                   ++ [x] ++
                   quickSort [y | y <- xs,
                                  y > x]
		
